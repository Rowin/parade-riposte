<?php
    require_once('wp-config.php');
    
    echo 'Connexion à la base de données...';
    try {
        $pdo = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASSWORD);
        echo "OK<br />";
        
        $requete = '
            UPDATE wp_posts
            JOIN wp_term_relationships 
                ON wp_term_relationships.object_id = wp_posts.ID
            JOIN wp_terms
                ON wp_term_relationships.term_taxonomy_id = wp_terms.term_id
            SET wp_posts.post_type = "result"
            WHERE wp_terms.slug = "resultats"';
        $result = $pdo->exec($requete);
        echo 'Changement du type des articles de catégorie résultat... Nombre d\'enregistrements affectés : '.$result;
    } catch (PDOException $e) {
        echo 'Echec de la connexion : '.$e->getMessage();
    }