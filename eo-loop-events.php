<?php
/**
 * The events loop. Used by archive-events.php, taxonomy-event-venue.php,
 * taxonomy-event-category.php and taxonomy-event-tag.php
 *
 * **************** NOTICE: *****************
 *  Do not make changes to this file. Any changes made to this file
 * will be overwritten if the plug-in is updated.
 *
 * To overwrite this template with your own, make a copy of it (with the same name)
 * in your theme directory.
 *
 * WordPress will automatically prioritise the template in your theme directory.
 * **************** NOTICE: *****************
 *
 * @package Event Organiser (plug-in)
 * @since   3.0.0
 */
?>
<main class="grid_8">
    <?php if (have_posts() ) : ?>
    <?php
    while ( have_posts() ) : the_post();
        eo_get_template_part('eo-loop-single-event');
    endwhile;
    ?>
    <?php else : ?>
    <!-- If there are no events -->
        <section>
            <h2>Oups !</h2>
            <p class="nothing">
                Il n'y a pas de post !
            </p>
        </section>
    <?php endif; ?>
</main>
