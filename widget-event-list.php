<?php
/**
 * Copyright 2016 Rowin <rowin@anhydride-acide.fr>
 *
 * This file is part of Parade-Riposte 2.
 * This file is based on Event Organiser (http://wp-event-organiser.com/) which is published under the GNU General Public License
 *
 *    Parade-Riposte 2 is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Parade-Riposte 2 is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with Parade-Riposte 2.  If not, see <http://www.gnu.org/licenses/>.
 **/
global $eo_event_loop,$eo_event_loop_args;

//The list ID / classes
$id      = ( $eo_event_loop_args['id'] ? 'id="'.$eo_event_loop_args['id'].'"' : '' );
$classes = $eo_event_loop_args['class'];
?>

<?php /*if ( $eo_event_loop->have_posts() ) :  ?>

	<ul <?php echo $id; ?> class="<?php echo esc_attr( $classes );?>" > 

		<?php while ( $eo_event_loop->have_posts() ) :  $eo_event_loop->the_post(); ?>

			<?php
				//Generate HTML classes for this event
				$eo_event_classes = eo_get_event_classes();

				//For non-all-day events, include time format
				$format = eo_get_event_datetime_format();
			?>

			<li class="<?php echo esc_attr( implode( ' ',$eo_event_classes ) ); ?>" >
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" ><?php the_title(); ?></a> <?php echo __( 'on','eventorganiser' ) . ' '.eo_get_the_start( $format ); ?>
			</li>

		<?php endwhile; ?>

	</ul>

<?php elseif ( ! empty( $eo_event_loop_args['no_events'] ) ) :  ?>

	<ul id="<?php echo esc_attr( $id );?>" class="<?php echo esc_attr( $classes );?>" > 
		<li class="eo-no-events" > <?php echo $eo_event_loop_args['no_events']; ?> </li>
	</ul>

<?php endif;*/?>
<?php if ($eo_event_loop->have_posts()) : ?>
    <h2>À venir</h2>
    <?php while ($eo_event_loop->have_posts()) :
        $eo_event_loop->the_post(); ?>
        <div class="event">
            <a href="<?php echo eo_get_permalink(); ?>">
                <span class="event-name"><?php the_title() ?></span>
                <span class="event-date"><?php echo eo_format_event_occurrence(); ?></span> - <span class="event-location"><?php echo eo_get_venue_name(); ?></span>
            </a>
        </div>
    <?php endwhile; ?>
<?php endif; ?>