<?php
/**
 * Copyright 2016 Rowin <rowin@anhydride-acide.fr>
 *
 * This file is part of Parade-Riposte 2.
 *
 *    Parade-Riposte 2 is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Parade-Riposte 2 is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with Parade-Riposte 2.  If not, see <http://www.gnu.org/licenses/>.
 **/
?>
<main class="grid_8">
    <?php if (have_posts()) : ?>
        <section class="sponsors">
            <h1><?php wp_title(""); ?></h1>
            <?php while (have_posts()) :
                the_post(); ?>
                <h2>
                    <?php the_title(); ?>
                </h2>
                <figure>
                    <?php the_post_thumbnail('sponsor-thumbnail'); ?>
                    <?php if ($link = get_post_meta(get_the_ID(), 'url', true)) : ?>
                        <figcaption><a href="<?php echo $link; ?>">Lien vers le site</a></figcaption>
                    <?php endif; ?>
                </figure>
            <?php endwhile; ?>
        </section>
    <?php else : ?>
        <section>
            <h2>Oups !</h2>
            <p class="nothing">
                Il n'y a pas de post !
            </p>
        </section>
    <?php endif; ?>
</main>
