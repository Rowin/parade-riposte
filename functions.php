<?php
/**
 * Copyright 2016 Rowin <rowin@anhydride-acide.fr>
 *
 * This file is part of Parade-Riposte 2.
 *
 *    Parade-Riposte 2 is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Parade-Riposte 2 is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with Parade-Riposte 2.  If not, see <http://www.gnu.org/licenses/>.
 **/

/* Thumbnail support */
add_theme_support('post-thumbnails');
set_post_thumbnail_size(620, 200, true);

add_image_size('sponsor-thumbnail', '200', '200', false);

/* Menu support */
function parade_riposte_menus_init()
{
    register_nav_menus(
        array(
            'header-menu'=> __('Menu de navigation principal'),
            'extra-menu' => __('Menu supplémentaire'),
        )
    );
}
add_action('init', 'parade_riposte_menus_init');

/* Widget support */
function parade_riposte_widget_init()
{
    register_sidebar(
        array(
        'name'          => __('Sidebar principale'),
        'id'            => 'main_sidebar',
        'before_widget' => '<section>',
        'after_widget'  => '</section>',
        )
    );
}
add_action('widgets_init', 'parade_riposte_widget_init');

/*Customs Post : Results*/
function parade_riposte_result_init()
{
    register_post_type(
        'result',
        array(
            'label'             => 'Résultats',
            'labels'            => array(
                'name'                  => 'Résultats',
                'singular_name'         => 'Résultat',
                'all_items'             => 'Tous les résultats',
                'add_new_item'          => 'Ajouter un résultat',
                'edit_item'             => 'Modifier le résultat',
                'new _item'             => 'Nouveau résultat',
                'view_item'             => 'Voir le résultat',
                'search_items'          => 'Chercher parmi les résultats',
                'not_found'             => 'Aucun résultat trouvé',
                'not_found_in_trash'    => 'Pas de résultat dans la corbeille',
            ),
            'public'            => true,
            'capability_type'   => 'post',
            'supports'          => array('title', 'editor', 'comments'),
            'has_archive'       => true,
            'menu_icon'         => 'dashicons-awards',
            'menu_position'     => 5,
        )
    );
}
add_action('init', 'parade_riposte_result_init');

/*Customs Post : Sponsors*/
function parade_riposte_sponsor_init()
{
    register_post_type(
        'sponsor',
        array(
            'label'             => 'Partenaires',
            'labels'            => array(
                'name'                  => 'Partenaires',
                'singular_name'         => 'Partenaire',
                'all_items'             => 'Tous les partenaires',
                'add_new_item'          => 'Ajouter un partenaire',
                'edit_item'             => 'Modifier le partenaire',
                'new _item'             => 'Nouveau partenaire',
                'view_item'             => 'Voir le partenaire',
                'search_items'          => 'Chercher parmi les partenaires',
                'not_found'             => 'Aucun partenaire trouvé',
                'not_found_in_trash'    => 'Pas de partenaire dans la corbeille',
            ),
            'public'            => true,
            'capability_type'   => 'post',
            'supports'          => array('title', 'thumbnail', 'custom-fields'),
            'has_archive'       => true,
            'menu_icon'         => 'dashicons-groups',
            'menu_position'     => 5,
        )
    );
}
add_action('init', 'parade_riposte_sponsor_init');

/*TinyMCE modification*/
function parade_riposte_tinymce_buttons($buttons)
{
    //Remove alignment buttons
    $remove = array(/*'alignleft','aligncenter', 'alignright'*/);
    $buttons[] = 'underline';
    return array_diff($buttons, $remove);
}
add_filter('mce_buttons', 'parade_riposte_tinymce_buttons');

function parade_riposte_register_tinymce_javascript($plugin_array)
{
    $plugin_array['table'] = content_url('/themes/parade-riposte/resources/js/table.min.js');
    return $plugin_array;
}
add_filter('mce_external_plugins', 'parade_riposte_register_tinymce_javascript');

function parade_riposte_tinymce_buttons2($buttons)
{
    //Add table buttons
    array_push($buttons, 'tableprops', 'tabledelete', '|', 'tableinsertrowbefore', 'tableinsertrowafter', 'tabledeleterow', '|', 'tableinsertcolbefore', 'tableinsertcolafter', 'tabledeletecol');
    $remove = array('underline', 'formatselect', 'alignjustify', 'forecolor', 'pastetext', 'removeformat', 'charmap', 'outdent', 'indent', 'wp_help');
    return array_diff($buttons, $remove);
}
add_filter('mce_buttons_2', 'parade_riposte_tinymce_buttons2');

/* ===============
 * Sponsors widget
 * =============== */
class parade_riposte_sponsors_widget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(
            'parade_riposte_sponsors_widget',
            'Parade Riposte - Sponsors',
            array(
                'description' => 'Ce widget permet d\'afficher les sponsors',
            )
        );
    }
    
    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);
        
        echo $args['before_widget'];
        
        if (!empty($title)) {
            echo $args['before_title'].$title.$args['after_title'];
        }
        
        $arguments = array(
            'offset' => 0,
            'posts_per_page' => $instance['number'],
            'orderby' => 'rand',
            'post_type' => 'sponsor',
        );
        $sponsor = get_posts($arguments);
        if ($sponsor) {
            foreach ($sponsor as $post) {
                echo get_the_post_thumbnail($post, 'sponsor-thumbnail', array('class' => 'sponsors'));
            }
        }
        
        echo $args['after_widget'];
    }
    
    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = 'Nos partenaires';
        }
        
        if (isset($instance['number'])) {
            $number = $instance['number'];
        } else {
            $number = 5;
        }
        ?>
            <label for="<?php echo $this->get_field_id('title'); ?>">Titre : </label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            
            <label for="<?php echo $this->get_field_id('number'); ?>">Nombre de sponsors à afficher : </label>
            <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" />
        <?php
    }
    
    public function update($new_instance, $old_instance)
    {
        return $new_instance;
    }
}

function parade_riposte_load_widgets()
{
    register_widget('parade_riposte_sponsors_widget');
}
add_action('widgets_init', 'parade_riposte_load_widgets');

/*Custom Headers support*/
$defaults = array(
    'height'         => 157,
    'flex-height'    => true,
    'width'          => 940,
    'flex-width'     => false,
    'uploads'        => true,
    'random-default' => true,
);
add_theme_support('custom-header', $defaults);
