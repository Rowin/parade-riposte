<?php
/**
 * Copyright 2016 Rowin <rowin@anhydride-acide.fr>
 *
 * This file is part of Parade-Riposte 2.
 *
 *    Parade-Riposte 2 is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Parade-Riposte 2 is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with Parade-Riposte 2.  If not, see <http://www.gnu.org/licenses/>.
 **/
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <title><?php bloginfo('name');
        wp_title(); ?></title>
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri().'/resources/css/reset.css'?>" type="text/css">
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri().'/resources/css/960_12_col.css'?>" type="text/css">
        <?php wp_head(); ?>
    </head>
    <body>
        <div class="wrap container_12">
            <header class="grid_12">
                <nav class="grid_12 alpha">
                    <img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>"/>
                    <?php wp_nav_menu(array( 'theme_location' => 'header-menu' )); ?>
                </nav>
            </header>