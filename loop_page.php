<?php
/**
 * Copyright 2016 Rowin <rowin@anhydride-acide.fr>
 *
 * This file is part of Parade-Riposte 2.
 *
 *    Parade-Riposte 2 is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    Parade-Riposte 2 is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with Parade-Riposte 2.  If not, see <http://www.gnu.org/licenses/>.
 **/
?>
<main class="grid_12">
<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <section>
            <h1>
                <?php the_title(); ?>
            </h1>
            <?php the_post_thumbnail(); ?>
            <article>
                <?php the_content(); ?>
            </article>
        </section>
    <?php endwhile; ?>
<?php else : ?>
    <p class="nothing">
        Il n'y a pas de post !
    </p>
<?php endif; ?>
</main>
